# Mass Lab Setup Using Ansible

Currently tested on Linux (ubuntu) machines over network. Installation steps only required for host machine (machine that you used to cloned this repo). Ansible required python and pip to be works, more info about [installing ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

## Installation

```bash
$ sudo apt update
$ sudo apt install software-properties-common
$ sudo add-apt-repository --yes --update ppa:ansible/ansible
$ sudo apt install ansible
```

## Usage

Check playbook syntax
```bash
ansible-playbook joindomain.yml --syntax-check
```
Execute playbook tasks, flag -i to specify inventory hosts location
```bash
ansible-playbook joindomain.yml -i hosts
ansible-playbook joindomain.yml -i hosts -c paramiko
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)